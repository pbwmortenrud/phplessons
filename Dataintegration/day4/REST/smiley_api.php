<?php
require_once 'class_api.php';
require_once 'db.php';

//FEATURE: 1: Fetch restauranter i $postnr, som ikke har været besøgt i X måneder. (75 måneder)
//TODO: 2: Fetch restauranter i $postnr, med minimum smiley, og max meter til mobilmast.
//[v]:Tilføj adresse i database og koble op til mastedatabasen.dk
//TODO: 3: Fetch restauranter i $postnr, som ligger indenfor en radius af X meter, en adresse. (DAWA api)
//:Koble op til Dawa api


class smiley_api extends API
{
    protected $con;

    public function __construct($request, $origin)
    {
        parent::__construct($request);
        $this->con = db::connect();

        /* TODO: IMPLEMENT AUTHENTICATION
            $APIKey = new APIKey();
            $User = new User();

            // Throw error if no API key detected, or if user not authenticated
            if (!array_key_exists('apiKey', $this->request)) {
                throw new Exception('No API Key provided');
            } else if (!$APIKey->verifyKey($this->request['apiKey'], $origin)) {
                throw new Exception('Invalid API Key');
            } else if (array_key_exists('token', $this->request) &&
                 !$User->get('token', $this->request['token'])) {

                throw new Exception('Invalid User Token');
            } */
    }

    // Endpoint for events. Returns all events
    protected function restauranter()
    {
        if ($this->method == 'GET') {
            $sql = 'SELECT * from spisesteder';
            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }

    protected function restauranteri($args)
    {
        $postnr = $args[0];

        if ($this->method == 'GET') {
            $sql = "SELECT navn, adresse, postnr, smiley from spisesteder WHERE postnr = $postnr ORDER BY smiley DESC";
            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }

    // Endpoint for single smiley on CVR
    protected function restaurant($args)
    {
        $cvr = $args[0];

        if ($this->method == 'GET') {
            $sql = "SELECT cvr as cvr_nr, 
            smiley, navn as firma_navn, 
            adresse, 
            seneste_kontrol 
            from spisesteder 
            where cvr=$cvr";
            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }
    // Endpoint for visits in X months
    protected function visit($args)
    {
        $postnr = $args[0];
        $months = $args[1];

        if (!is_null($months)) {

            if ($this->method == 'GET') {
                $sql = "SELECT cvr as cvr_nr, 
            smiley, navn as firma_navn, 
            seneste_kontrol
            from spisesteder 
            where postnr=$postnr
            && seneste_kontrol <= DATE_SUB(CURDATE(), INTERVAL $months MONTH)"; 
            //Kilde: https://stackoverflow.com/questions/13912035/display-record-older-than-3-months-in-sql
                $results = $this->con->query($sql);

                return mysqli_fetch_all($results, MYSQLI_ASSOC);
            } else {
                return "Only accepts GET requests";
            }
        }
    }

    protected function novisit($args)
    {
        $postnr = $args[0];

            if ($this->method == 'GET') {
                $sql = "SELECT cvr as cvr_nr, 
            smiley, navn as firma_navn, 
            seneste_kontrol
            from spisesteder 
            where postnr=$postnr
            && seneste_kontrol = '0000-00-00'"; 
                $results = $this->con->query($sql);

                return mysqli_fetch_all($results, MYSQLI_ASSOC);
            } else {
                return "Only accepts GET requests";
            }
        
    }




    // Endpoint for MUSIK events in a postcode //!Not used
    protected function musik($postnr)
    {
        $postnr = $postnr[0];

        if ($this->method == 'GET') {
            $sql = "SELECT eventtitle.title, 
                    event.eventid, 
                    eventlocation.location_city, 
                    eventlocation.location_postcode, 
                    event.category, 
                    event.category_id 
                FROM eventlocation 
                INNER JOIN event on event.id = eventlocation.eventid 
                INNER JOIN eventtitle ON event.id = eventtitle.eventid 
                WHERE eventlocation.location_postcode = '$postnr' 
                && event.category = 'Musik' 
                && eventtitle.language = 'DK';";
            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }
    // Endpoint for BYLIV events in a postcode //!Not used
    protected function byliv($postnr)
    {
        $postnr = $postnr[0];

        if ($this->method == 'GET') {
            $sql = "SELECT eventtitle.title, 
                    event.eventid, 
                    eventlocation.location_city, 
                    eventlocation.location_postcode, 
                    event.category, 
                    event.category_id 
                FROM eventlocation 
                INNER JOIN event on event.id = eventlocation.eventid 
                INNER JOIN eventtitle ON event.id = eventtitle.eventid 
                WHERE eventlocation.location_postcode = '$postnr' 
                && event.category = 'Byliv' 
                && eventtitle.language = 'DK';";
            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }
}
