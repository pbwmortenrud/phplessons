<?php
require_once 'class_api.php';
require_once 'db.php';

class events_api extends API
{
    protected $con;

    public function __construct($request, $origin)
    {
        parent::__construct($request);
        $this->con = db::connect();

        /* TODO: IMPLEMENT AUTHENTICATION
            $APIKey = new APIKey();
            $User = new User();

            // Throw error if no API key detected, or if user not authenticated
            if (!array_key_exists('apiKey', $this->request)) {
                throw new Exception('No API Key provided');
            } else if (!$APIKey->verifyKey($this->request['apiKey'], $origin)) {
                throw new Exception('Invalid API Key');
            } else if (array_key_exists('token', $this->request) &&
                 !$User->get('token', $this->request['token'])) {

                throw new Exception('Invalid User Token');
            } */
    }

    // Endpoint for events. Returns all events
    protected function events()
    {
        if ($this->method == 'GET') {
            $sql = 'SELECT * from event';
            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }

    // Endpoint for single events
    protected function event($id)
    {
        $id = $id[0];

        if ($this->method == 'GET') {
            $sql = "SELECT * from event where id=$id";
            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }

    //TODO: Get alle musikevents i århus postnr(8000). 

    // Endpoint for MUSIK events in a postcode
    protected function musik($postnr)
    {
        $postnr = $postnr[0];

        if ($this->method == 'GET') {
            $sql = "SELECT eventtitle.title, 
                    event.eventid, 
                    eventlocation.location_city, 
                    eventlocation.location_postcode, 
                    event.category, 
                    event.category_id 
                FROM eventlocation 
                INNER JOIN event on event.id = eventlocation.eventid 
                INNER JOIN eventtitle ON event.id = eventtitle.eventid 
                WHERE eventlocation.location_postcode = '$postnr' 
                && event.category = 'Musik' 
                && eventtitle.language = 'DK';";
            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }
    // Endpoint for MUSIK events in a postcode
    protected function byliv($postnr)
    {
        $postnr = $postnr[0];

        if ($this->method == 'GET') {
            $sql = "SELECT eventtitle.title, 
                    event.eventid, 
                    eventlocation.location_city, 
                    eventlocation.location_postcode, 
                    event.category, 
                    event.category_id 
                FROM eventlocation 
                INNER JOIN event on event.id = eventlocation.eventid 
                INNER JOIN eventtitle ON event.id = eventtitle.eventid 
                WHERE eventlocation.location_postcode = '$postnr' 
                && event.category = 'Byliv' 
                && eventtitle.language = 'DK';";
            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }
}
