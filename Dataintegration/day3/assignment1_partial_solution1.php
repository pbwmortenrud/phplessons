<?php
/* 
** CVR API INTEGRATION 
*/

//TODO: find mobilmaster tæt på. mastedatabasen.dk. 
/**
 * 
 * The getDistance() function helps to get the distance between two address using PHP.
 *Specify your Google Maps API key.
 *Change the format of the addresses.
 *Initiate the Geocoding API request and specify the output format and address to fetch the geographic data.
 *Retrieve the latitude and longitude from the geodata.
 * 
 * 
 * 
 */

// URI to resource
$vat = 36174269;
$url = "https://cvrapi.dk/api?search=" . $vat . "&country=dk"; //Kahytten

// Initiate the connection
$cn = curl_init($url);

// Use GET
curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');

// Execute
// $page = curl_exec($cn);

// $page = json_decode($page);

// var_dump($page->name);
// var_dump($page->vat);
// var_dump($page->enddate);
// var_dump($page);

/* 
** END CVR API INTEGRATION 
*/



// Check if file exists
if (file_exists('sample_files/allekontrolresultater.xml')) {

    // load file with simplexml_load_file
    $xml = simplexml_load_file('sample_files/allekontrolresultater.xml');
}



// Pretty print
echo '<pre>';

$allowed = array('7100', '7000', '6100', '6000');
$allowed_vat = array($vat); //array of cvr numbers


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "restauranter";

$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}



/* 
! ETL - Extract / Transform / Load
* Extract - træk data fra forskellige kilder
* Transform kan være et filter. Indsæt af nye felter
* Load, smid det ind i en database.
 */

foreach ($xml as $record) {
    // echo '<div>';
    if (
        in_array(xml_attribute($record, 'postnr'), $allowed)
        // && in_array(xml_attribute($record, 'cvrnr'), $allowed_vat)
    ) {

        //var_dump($record);

        $postnr = htmlspecialchars((string)$record->attributes()->postnr);
        $branchekode = htmlspecialchars((string)$record->attributes()->brancheKode);
        $navn = trim(htmlspecialchars((string)$record->attributes()->navn1));
        $adresse = trim(htmlspecialchars((string)$record->attributes()->adresse1));
        $seneste_kontrol = htmlspecialchars((string)$record->attributes()->seneste_kontrol_dato);
        $smiley = (int)$record->attributes()->seneste_kontrol;
        $cvrnr = (string)$record->attributes()->cvrnr;
        $geo_lng = (string)$record->attributes()->Geo_Lng;
        $geo_lat = (string)$record->attributes()->Geo_Lat;

        //TODO: Check if exists in CVR API (check enddate not null)
        // if ($page->enddate == null) {echo 'exists<br>';}

        //TODO: Set $navn = cvrAPI navn
        // var_dump($navn);
        // var_dump($page->name);
        // $navn = $page->name;

        //TODO: Filter for date, 2015 & 2016 

        $filter_dato_1 = '2015-01-01';
        $filter_dato_2 = '2017-01-01';
        $filter_smiley = 1;
        $color_1 = 'green';
        $color_2 = 'yellow';
        $color_3 = 'red';
        $color = 'black';
        if (strtotime($seneste_kontrol) >= strtotime($filter_dato_1) && strtotime($seneste_kontrol) < strtotime($filter_dato_2)) {

            //TODO: Filter on smiley 1 to 4.

            if ($smiley >= $filter_smiley) {

                switch ($smiley) {
                    case 1:
                        $color = $color_1;
                        break;
                    case 2:
                        $color = $color_2;
                        break;
                    case 3:
                        $color = $color_3;
                        break;
                    case 4:
                        $color = $color_3;
                        break;
                    case 5:
                        $color = $color_3;
                        break;
                    default:
                        $color = $color;
                        break;
                }

                //echo '<span style="background: ' . $color . ';">';
                //echo '[' . $smiley . ']' . '</span>' . 'CVR: ' . $cvrnr . ', SENESTE_KONTROL:' . $seneste_kontrol . ', ' . 'NAVN: ' . $navn . '<br>';
            }
        }



        $sql = "INSERT INTO spisesteder (cvr, postnr, branchekode, navn, adresse, seneste_kontrol, smiley, geo_lat, geo_lng) VALUES ('"
            . $cvrnr . "', '"
            . $postnr . "', '"
            . $branchekode . "', '"
            . $navn . "', '"
            . $adresse . "', '"
            . $seneste_kontrol . "', '"
            . $smiley . "', '"
            . $geo_lat . "', '"
            . $geo_lng . "')";

        //var_dump($sql);

             $conn->query($sql);
    }

    // echo '</div>';
}

$conn->close();

function xml_attribute($object, $attribute)
{
    if (isset($object[$attribute]))
        return (string) $object[$attribute];
}
