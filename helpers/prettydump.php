<?php

function pretty_dump($string){
    echo '<pre>';
    var_dump($string);
    echo '</pre>';
};

function pretty_echo($string){
    echo '<p>';
    echo($string);
    echo '</p>';
};

function pretty_dump2($data){
    highlight_string("<?php\n\$data =\n" . var_export($data, true) . ";\n?>");
};

function dd($data){
    highlight_string("<?php\n " . var_export($data, true) . "?>");
    echo '<script>document.getElementsByTagName("code")[0].getElementsByTagName("span")[1].remove() ;document.getElementsByTagName("code")[0].getElementsByTagName("span")[document.getElementsByTagName("code")[0].getElementsByTagName("span").length - 1].remove() ; </script>';
    die();
  }