<?php

// Set error reporting to true!
ini_set('error_reporting', E_ALL);

// 1. Create the code that displays the 3 to the power table. ie. 3,6,9,12 ....
// up to 100

// 2. Create the code that creates the following
// *
// **
// ***
// ****
// *****
// ******
// *******
// ********
// *********
// **********

// 3. Create the code that creates
// *+++++++++
// **++++++++
// ***+++++++
// ****++++++
// *****+++++
// ******++++
// *******+++
// ********++
// *********+

// 4. Create the code that creates
// ++++++++++
// **********
// ++++++++++
// **********
// ++++++++++
// **********
// ++++++++++
// **********
// ++++++++++
// **********

// 5. Create the code that displays 4 to the power table up to 100, but skips all numbers divisible by 10. Ie. 4,8,12,16, 24, 28 ...

// 6. Bonus assignment
// Create the code that creates the fibonacci order
// 1 1 2 3 5 8 13 21 34 55 89 etc. up to 500