<?php
//Cookies

include '../helpers/prettydump.php';


// setcookie('test_cookie', 'test value', time() + 10);
// pretty_dump($_COOKIE);


//setcookie('test_cookie', date("D W, d M Y h:m:s", time()), time() + 3600);

// echo $_COOKIE['test_cookie'];

//Sessions

session_start(); //finds session, if already exists, or create new session. create cookie PHPSESSID

//you can set variables in $_SESSION as an associative array
$_SESSION['name'] = 'Morten';
pretty_dump($_SESSION); 

//unset 1 variabel
unset($_SESSION['name']);

//unset hele session ie. at logout
session_unset();
session_destroy();

pretty_dump($_SESSION); 


