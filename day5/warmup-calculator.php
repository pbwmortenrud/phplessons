<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Simpel Lommeregner</title>
</head>

<?php
/*
*
*   Lav en lommeregner ved hjælp af nedenstående formular. Du skal tage de indtastede værdier fra 
*   first_num og second_num og printe det rigtige resultat i feltet "result".
*   Du skal tage højde for hvilken operator der er valgt og lave udregningen ud fra den
*
*/
include '../helpers/prettydump.php';

//Instantiate
$first_num = null;
$second_num = null;
$result = null;

if ($_POST) {

    $first_num = (int) $_POST['first_num'];
    $second_num = intval($_POST['second_num']);
    $operator = $_POST['operator'];
    $result = null;

    switch ($operator) {
        case 'Plus':
            $result = $first_num + $second_num;
            break;
        case 'Minus':
            $result = $first_num - $second_num;
            break;
        case 'Gange':
            $result = $first_num * $second_num;
            break;
        case 'Divider':
            $result = $first_num / $second_num;
            break;
        default:
            $result = null;
            break;
    }
}

// if($_POST['operator'] == "Plus"){

//     $result = $first_num + $second_num;
// }
// else if($_POST['operator'] == "Minus"){

//     $result = $first_num - $second_num;
// }
// else if($_POST['operator'] == "Gange"){

//     $result = $first_num * $second_num;
// }
// else if($_POST['operator'] == "Divider"){

//     $result = $first_num / $second_num;
// }

// $_POST['result'] = $result;

// pretty_dump($_POST);

?>

<body>
    <div id="page-wrap">
        <h1>PHP - Simpel Lommeregner</h1>
        <form action="" method="post">
            <p>
                <input type="number" name="first_num" id="first_num" required="required" value="<?= $first_num ?>" /> <b>Første tal</b>
            </p>
            <p>
                <input type="number" name="second_num" id="second_num" required="required" value="<?= $second_num ?>" /> <b>Andet tal</b>
            </p>
            <p>
                <input readonly="readonly" name="result" value="<?= $result ?>"> <b>Resultat</b>
            </p>
            <input type="submit" name="operator" value="Plus" />
            <input type="submit" name="operator" value="Minus" />
            <input type="submit" name="operator" value="Gange" />
            <input type="submit" name="operator" value="Divider" />
        </form>
    </div>
</body>

</html>