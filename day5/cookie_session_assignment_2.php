<?php

ini_set('error_reporting', E_ALL);
include '../helpers/prettydump.php';

session_start();
// $_SESSION['auth'] = isset($_SESSION['auth']) ? null : false; 
$username_correct = 'morten';
$password_correct = 'admin';
$username_input = null;
$password_input = null;

/*
* 1.0 Create a login form, that validates the user
* If the user is authenticated, set the session variables 'auth' to true, 
* and the 'username', to the username
*/
if ($_SESSION['auth'] != true) {
    echo '<h2>User is not logged in, please log in</h2>';
    if ($_POST) {
        if ($_POST['log'] == "LOGIN") {

            $username_input = $_POST['username'];
            $password_input = $_POST['password'];

            if ($username_correct == $username_input && $password_correct == $password_input) {
                pretty_dump($_POST);
                $_SESSION['auth'] = true;
                $_SESSION['username'] = $username_input;
            } else
                echo '<h2>Wrong input, try again!</h2>';
        }
    }
} else {
    echo '<h2>Welcome <span style="color: green;">' . $_SESSION['username'] . '</span></h2>';
}
// pretty_dump($_SESSION['auth']);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>

<body>

    <form action="" method="post">
        <input type="text" name="username" id="username" placeholder="Username">
        <input type="password" name="password" id="password" placeholder="Password">
        <input type="submit" name="log" value="LOGIN">
        <input type="submit" name="log" value="LOGOUT">
    </form>

</body>

</html>




<?php
// 1.1 Create a logout function
// If the logout button is pressed, set the session variables 
// 'auth' to false, and the 'username' to null

if ($_POST) {
    if ($_POST['log'] == 'LOGOUT') {
        $_SESSION['username'] = 'null';
        $_SESSION['auth'] = false;
    }
}

pretty_dump($_SESSION);

// 1.2 Create a Welcome page. Only display the welcome page if the user 
// is authenticated and a username exists
?>