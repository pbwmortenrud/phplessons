<?php

include '../helpers/prettydump.php';
require 'Book.php';

// Assignment 1.1:
// Create a book class
// The book must contain information about 
// Title of the book
// Year it was published
// ISBN
// The book ID
// Loan status
// Lender ID   
// Language


// Assignment 1.2: Create the code to make an instance of the book class.

// Assignment 1.3:
//  Create the accessors and mutators of each of the properties of the object

// Assignment 1.4:
// Create 5 bookobject and add them to an array
$book1 = new Book(1, "Seven Husbands of Evelyn Hugo : Tiktok made me buy it!", '9781398515697', "English", "14 Oct 2021");
$book2 = new Book(2, "Small Pleasures : Longlisted for the Women's Prize for Fiction 2021", '9781474613903', "English", "29 Apr 2021");
$book3 = new Book(3, "The Weekend : A Sunday Times 'Best Books for Summer 2021'", '9781474612999', "English", "24 Jun 2021");
$book4 = new Book(4, "The Fortnight in September", '9781982184780', "English", "07 Sep 2021");
$book5 = new Book(5, "The Expendable Man", '9781590174951', "English", "03 Jul 2012");

//Get an error!
// $book6 = new Book(6, "NOT A BOOK", '97810174951', "Volapyk", "03 Jul 2012");

// echo $book6;

$books_array = array($book1, $book2, $book3, $book4, $book5,);

// Assignment 1.5:
// Print out of books in the array

?>


<ul>
    <?php 
    foreach ($books_array as $value) {
        echo $value;
    }
    ?>
</ul>


