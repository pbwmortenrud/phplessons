<?php
include '../helpers/prettydump.php';

//require the class
require 'Album.php';

$media = new Media(1, "Medie", "ME", "YOU");

$queen = new Album(
    2,
    "Queen - greatest hits",
    "Queen",
    "Universal Music Group",
    "Queen_cover.png",
    [
        'track_1', 
        'track_2',
         'track_3'
    ]
);

pretty_dump($media);
pretty_dump($queen);
$media->print_title();
$queen->print_title();