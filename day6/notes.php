<?php

/**
 * En Class er et stykke kode, som kan defineres som en opskrift. 
 * Den har nogle egenskaber og nogle metoder som kan genbruges.
 * Det kan være en user-class, som har et brugernavn og
 * en metode til at logge ind og ud. 
 */

/**
 * Objects er instanser af Classes.
 */

/**
 * Good practise of classes
 * 
 * Naming: PascalCase || class.object.php 
 */

/**
 * public function __construct() is a MAGIC function, 
 * defined by __ 
 * It means, it runs automatically. 
 * __toString() can be overloaded.
 */

/**
 * STATIC FUNCTIONS
 *     public static function is_variable_numeric($val)
 *   {
 *       return is_numeric($val);
 *   }
 *
 *   public static function format_integer($val)
 *   {
 *       if (self::is_variable_numeric($val))
 *           return intval($val);
 *       else return false;
 *   }
 * 
 */
