<?php

require_once 'Media.php';

class Album extends Media
{

    public $album_cover;
    public $tracks;

    public function __construct($id, $title, $author, $publisher, $album_cover, $tracks)
    {
        $this->id = $id;
        $this->title = $title;
        $this->author = $author;
        $this->publisher = $publisher;
        $this->album_cover = $album_cover;
        $this->tracks = $tracks;
    }

    //redeclare inherited method
    public function print_title(){
        echo '<br>the title of this album is: '.$this->title;
    }


}
