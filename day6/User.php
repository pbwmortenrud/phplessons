<?php

class User
{
    //Properties
    private $id;
    private $username;
    protected $password;
    public $firstname;
    public $lastname;

    public function __construct($id, $username, $password, $firstname, $lastname)
    {
        $this->id = $this->set_id($id);
        $this->password = password_hash($password, PASSWORD_BCRYPT);
        $this->username = $username;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }


    public function print_id()
    {
        echo '<div>My ID is: ' . $this->id . ' </div>';
    }

    public function print_username()
    {
        echo '<div>My Username is: ' . $this->username . ' </div>';
    }

    //Accessors and mutators (get/set)
    public function get_id()
    {
        return $this->id;
    }
    public function set_id($id)
    {
        $this->id = (int) $id;
    }

    public function get_JSON()
    {
        return "{'user' : 
            {
                'username' : '$this->username',
                'firstname' : '$this->firstname', 
                'lastname' : '$this->lastname' 

        }}";
    }

    public static function is_variable_numeric($val)
    {
        return is_numeric($val);
    }

    //called by User::format_integer($val)
    //Static methods called inhouse by self, not $this.
    public static function format_integer($val)
    {
        if (self::is_variable_numeric($val))
            return intval($val);
        else return false;
    }
}
