<?php

class Book
{

    private $id;
    private $title;
    private $isbn13;
    private $loan_status;
    private $lender_id;
    private $language;
    private $publish_year;

    public function __construct($id, $title, $isbn13, $language, $publish_year)
    {
        $this->id = $id;
        $this->title = $title;
        $this->isbn13 = $this->set_isbn13($isbn13);
        $this->loan_status = false;
        $this->lender_id = null;
        $this->language = $language;
        $this->publish_year = $publish_year;
    }

    public function __toString()
    {
        return <<<EOT
        <li> {$this->get_title()}, ISBN {$this->get_isbn13()} ,[<i> {$this->get_publish_year()} </i>] </li>
        EOT;
    }

    public function get_id()
    {
        return $this->id;
    }
    public function set_id($id)
    {
        $this->id = (int) $id;
    }
    public function get_title()
    {
        return $this->title;
    }
    public function set_title($string)
    {
        $this->id = $string;
    }
    public function get_isbn13()
    {
        return $this->isbn13;
    }
    public function set_isbn13($isbn)
    {
        ////! review turnary operator
        $this->isValidIsbn13($isbn) ? $this->isbn13 = $isbn : $this->error('isbn', $isbn);
    }
    public function get_loan_status()
    {
        return $this->loan_status;
    }
    public function set_loan_status()
    {
        $this->id = '';
    }
    public function get_lender_id()
    {
        return $this->lender_id;
    }
    public function set_lender_id()
    {
        $this->id = '';
    }
    public function get_language()
    {
        return $this->language;
    }
    public function set_language()
    {
        $this->id = '';
    }
    public function get_publish_year()
    {
        return $this->publish_year;
    }
    public function set_publish_year()
    {
        $this->id = '';
    }

    
    private function isValidIsbn13($isbn)
    {
        if (strlen($isbn) !== 13) return false;
        $check = 0;
        for ($i = 0; $i < 13; $i += 2) {
            $check += (int)$isbn[$i];
        }
        for ($i = 1; $i < 12; $i += 2) {
            $check += 3 * $isbn[$i];
        }
        return (0 === ($check % 10)) ? 2 : false;
    }

    private function error($e, $variable)
    {
        echo "<section style='background-color: red; color: white;'>";
        echo "<h2>Fejlkode: ".$e."</h2>";
        switch ($e) {
            case 'isbn':
                echo "<b>" .$variable."</b> er ikke et gyldigt ISBN13 nummer!";
                break;

            default:
                echo "Der skete en fejl!";
                break;
        }
        echo "</section>";
    }

    
}
