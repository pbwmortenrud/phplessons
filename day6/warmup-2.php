<!DOCTYPE html>
<html lang="da">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>

<?php

// Ved at bruge funktionen date(), time() og strtotime(). Udskriv følgende i browseren:

include '../helpers/prettydump.php';


// 1. Dags dato i formatet DD-MM-YYYY (18-02-2022)
pretty_echo(date('d-m-Y'));

// 2. Det nuværende tidspunkt i formatet TT:MM:SS (10:54:16)
pretty_echo(date("H:i:s", strtotime("now")));

// 3. Dags dato + det nuværende tidspunkt i formatet DD-MM-YYYY TT:MM:SS
pretty_echo(date("d-m-Y H:i:s", strtotime("now")));

// 4. Udskriv dennes uges ugenr.
pretty_echo(date('W'));

// 5. Hvilken ugedag det var på din fødselsdag. Ugedagen skal være på dansk (Mandag, Tirsdag, Onsdag osv.)
$days = [
    'Sun' => 'Søndag',
    'Mon' => 'Mandag' ,
    'Tue' => 'Tirsdag', 
    'Wed' => 'Onsdag',
    'Thu' => 'Torsdag',
    'Fri' => 'Fredag',
    'Sat' => 'Lørdag'
];
// pretty_dump($days);
// pretty_echo($days[date('w', strtotime("Feb 27 2022"))]);
pretty_echo($days[date('D', strtotime("Feb 27 2022"))]);

// 6. Hvilken dato var det for 40.000 minutter siden?
pretty_echo(date('d-m-Y',strtotime("-40000 seconds")));
