<?php
//overclass for book
class Media
{
    //use protected, when doing inheritance
    protected $id;
    protected $title;
    protected $author;
    protected $publisher;

    public function __construct($id, $title, $author, $publisher)
    {
        $this->id = $id;
        $this->title = $title;
        $this->author = $author;
        $this->publisher = $publisher;
    }

    public function print_title(){
        echo '<br>the title of this media is: '.$this->title;
    }

    public function set_id($value){
         $this->id = $value;
    }
    public function get_id(){
        return $this->id;
    }
}
