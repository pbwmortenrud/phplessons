<?php

// Set error reporting to true!
ini_set('error_reporting', E_ALL);

// 1.0  Create an array based on the following textstring
//      "Before diving into creating and using arrays, its 
//       worth taking a moment to explore the concept of an array. 
//       Powerful though they are, arrays in PHP are easy to create."

$string = 'Before diving into creating and using arrays, its worth taking a moment to explore the concept of an array. Powerful though they are, arrays in PHP are easy to create.';

$string = str_replace([',','.'], '', $string);

$string_array = explode(' ', $string);

echo '<pre>';
var_dump($string_array);
echo '</pre>';

// 1.1  Figure out if the word: explode exists in the array

$needle = 'explode';
$explode_exists = in_array($needle, $string_array);

echo '<br>';
echo 'The word '.$needle.' is '.($explode_exists ? 'found' : 'not found').' in the array';


// 1.2  Sort the array alphabetically and print it on screen

// 1.3  Print the amount of words in the array

// 1.4  Print only the first 5 words in the array

// 1.5  Create a list of unique words in the text.

// 1.6  Bonus assignment:
//      Display which words exist in the above text, that do not exists in the following:
//      "As a developer, arrays are a very Powerful tool at our disposal"