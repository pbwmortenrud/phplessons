<?php

// Set error reporting to true!
ini_set('error_reporting', E_ALL);

// 1.0  Create an indexed array containing the following:
//      blue, red, yellow, brown, black
//      Print out the yellow

// 1.1  Remove the red color from the array, and print out the array

// 1.2  Change the brown color to purple, and print out the array

// 1.3  Add cyan to the array

// 1.4  Create an associative array containing the following:
//      bus = greyhound, 
//      train = ic4, 
//      plane = boing 747, 
//      bicycle = kildemoes, 
//      car = ford
//      Print out the car

// 1.5  Change car to be a toyota, and print out the array

// 1.6  Re-add the ford to the array

// Bonus assignments
// 1.7  Add an array to the bus, containing three busses: 
//      greyhound, citybus and xbus.

// 1.8  Print out only the busses


