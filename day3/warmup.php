<?php 

include '../helpers/prettydump.php';

/*
*   1.
*   Lav en funktion der kan tjekke om et ord er et palindrom. Et palindrom er en sætning eller et ord der kan læses fra begge retninger som f.eks. Anna, Regninger, "Den laks skal ned"
*   Eksempel: check_palindrome('anna') == true
*   Eksempel: check_palindrome('pølse') == false

*   Hint: er du i tvivl, så prøv at Google "php reverse string"
*/
    echo '<h2> Opgave 1 </h2>';

    $test_string = 'abba';
    $test_stringA = 'abe';
    $test_stringB = 'A nut for a jar of tuna';

    function check_palindrome($string){
        //remove signs and whitespace
        $string = str_replace([',','.',' '],'', $string);

        //Making string to lowercase before checking.
        //using PHP string reverse function to reverse the string
        $is_palindrome = false;
        if(strtolower($string) == strrev(strtolower($string))) 
            $is_palindrome = true;
        return $is_palindrome;
    }
    echo '<p> is <strong>'.$test_string.'</strong> a palindrome? :'.(check_palindrome($test_string) ? 'true' : 'false');
    echo '<p> is <strong>'.$test_stringA.'</strong> a palindrome? :'.(check_palindrome($test_stringA) ? 'true' : 'false');
    echo '<p> is <strong>'.$test_stringB.'</strong> a palindrome? :'.(check_palindrome($test_stringB) ? 'true' : 'false');


/*
*
*   2.
*   Lav en funktion der kan capitalize hvert ord i en string
*   Eksempel: capitalize('hello world!') == 'Hello World'
*   Hint: Google er din ven
*/
echo '<h2> Opgave 2 </h2>'; 


/*
*
*   Bonus
*   Lav en funktion der kan tjekke om to sætninger eller ord et et anagram. Et anagram er når de samme bogstaver bliver brugt i to sætninger eller ord.
* 
*   Eksempel: check_anagram('rail safety', 'fairy tales') == true
*   Eksempel: check_anagram('roast beef', 'goat roast') == false
*   Eksempel: check_anagram('Elvis, 'lives') == false
*   
*/

//count_chars
echo '<h2> Bonus </h2>'; 


check_anagram('rail safety', 'fairy tales');

function check_anagram($string_1, $string_2){

    $result = false;

    $string_1 = str_replace([',','.',' '],'', strtolower($string_1));
    $string_2 = str_replace([',','.',' '],'', strtolower($string_2));

    $string_array_1 = str_split($string_1);
    $string_array_2 = str_split($string_2);

    pretty_dump($string_array_1);
    pretty_dump($string_array_2);

    natsort($string_array_1);
    natsort($string_array_2);

    pretty_dump($string_array_1);
    pretty_dump($string_array_2);

    $string_compare_1 = implode('', $string_array_1);
    $string_compare_2 = implode('', $string_array_2);

    pretty_dump($string_compare_1);
    pretty_dump($string_compare_2);

    $result = ($string_compare_1 === $string_compare_2);

    return $result;
}