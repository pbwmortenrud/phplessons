<?php

include '../helpers/prettydump.php';


//$_GET
//$_POST
//$_SERVER

//$_COOKIE
//$_SESSION

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP form</title>
</head>
<body>
    <h2>form in php</h2>

    <form action="form_handler.php" method="POST">
    <div>
        <input name="name"type="text" placeholder="name">
    </div>    
    <div>
        <input name="age"type="text" placeholder="age">
    </div>    
    <input type="submit" value="Send!">

    </form>

    <?php

    // if($_GET){
    //     echo 'Hello my name is '.$_GET['name'].' and I\'m '.$_GET['age']. ' years old';
    // }

    pretty_dump($_GET);
    pretty_dump($_POST);
    pretty_dump($_SERVER);

    ?>

</body>
</html>

