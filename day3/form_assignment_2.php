<!DOCTYPE html>
<!--
Created by Per Toft
-->
<?php
include '../helpers/prettydump.php';

pretty_dump($_GET);

if(!empty($_GET['error'])) {
    echo '<div style="background-color:red; padding:.25em;"><span style="color:white;">
    Der er sket en fejl.<br>
    Der er fejl i følgende. felter: <br>'
    . implode(', ', $_GET['error']).
    '</span><br></div>';
}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // 1.0  Create a contactform containing name, address, city, zipcode
        //      Send it to a form handler
        //      If any of the form fields are not filled out, return to this page and 
        //      display an error containing information on how to prevent the error

        // 1.1  Preserve the input for the user


        ?>

        <form action="form_handler_2.php" method="post">
            <div><input type="text" name="name" id="name" placeholder="name" value="<?= !empty($_GET['user_input']['name']) ? $_GET['user_input']['name'] : '' ?>"></div>
            <div><input type="text" name="address" id="address" placeholder="address" value="<?= !empty($_GET['user_input']['address']) ? $_GET['user_input']['address'] : '' ?>"></div>
            <div><input type="text" name="city" id="city" placeholder="city" value="<?= !empty($_GET['user_input']['city']) ? $_GET['user_input']['city'] : '' ?>"></div>
            <div><input type="text" name="zipcode" id="zipcode" placeholder="zipcode" value="<?= !empty($_GET['user_input']['zipcode']) ? $_GET['user_input']['zipcode'] : '' ?>"></div>
            <div><input type="submit" value="Send!"></div>
        </form>
    </body>
</html>
