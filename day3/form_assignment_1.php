<?php
 include '../helpers/prettydump.php';

 phpinfo();
/*
*
        Lav en formular der indeholder felterne: Navn, Tlf og Email samt et skjult felt der indeholder sidens URL.
        Udskriv de indtastede værdier på siden efter at formularen er sendt.
*
*/

?>

<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP forms 1</title>
</head>
<body>
        <form action="" method="post">

        <div><input type="text" name="name" id="name" placeholder="name"></div>
        <div><input type="text" name="phone" id="phone" placeholder="phone number"></div>
        <div><input type="email" name="email" id="email" placeholder="email@mail.com"></div>

        <input type="hidden" name="url" value="<?php echo $_SERVER['HTTP_ORIGIN'].$_SERVER['REQUEST_URI']; ?>">
        
        <input type="submit" value="send!">
        </form>
</body>
</html>

<?php 
         
        // pretty_dump($_SERVER['REQUEST_URI'])

        //TIP: '<?php echo'  == '<?='
?>
