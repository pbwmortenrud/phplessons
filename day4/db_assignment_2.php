<?php


    // 1.0 Connect to your database from assigment 1

    include 'db_connection.php';
    
    // 1.1 Create a Sign up form, containing the fields from the user table (excluding the ID column)
    //     The form should include a <select> of clients where the client name is the label, and the id is the value

    // 1.2 Create a form handler which will handle the submission

    // 1.3 Validate that all fields are filled out, send the user back to the sign up form and display an error if 
    //     the validation fail

    // 1.4 If all fields are validated, insert the user into the database

    // 1.5 Display an error if you submit a username which already exists in the database
