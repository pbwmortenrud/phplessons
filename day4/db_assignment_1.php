<?php



    // 1.0 Create an empty database with phpmyadmin, call it whatever you like

    // 1.1 Connect to your database
    // Variables that hold the values for the connection
    $server = 'localhost';
    $database = 'phpmyadmin_dump';
    $username = 'root';
    $password = '';
    
    $connection = new mysqli($server, $username, $password, $database);


    // 1.2 Print a message that contains the credentials when connected or an error message if the connection failed
    echo '<h2> 1.2 Print a message that contains the credentials when connected or an error message if the connection failed </h2>';
    echo ($connection->connect_errno) ? 'error: '.$connection->connect_errno : 'successfully connected';


    // 1.3 Import dump.sql into your database
    /* DONE */

    // 1.4 Retrieve all users from the database and display them in an HTML table
    echo '<h2> 1.4 Retrieve all users from the database and display them in an HTML table </h2>';

    $sql = "SELECT username FROM users";
    $result = $connection->query($sql);
    
    echo '<table><tr><th style="border: black solid 2px; background: #EEE;">Users</th></tr>';
    foreach ($result as $row) {
        echo '<tr><td style="border: black solid 1px;">'.$row['username'].'</td></tr>';
    }
    echo '</table>';


    
    // 1.5 Retrieve all clients from the database and display them in an HTML table
    echo '<h2> 1.5 Retrieve all clients from the database and display them in an HTML table </h2>';

    $sql2 = "SELECT name FROM clients";
    $result2 = $connection->query($sql2);
    
    echo '<table><tr><th style="border: black solid 2px; background: #EEE;">Clients</th></tr>';
    foreach ($result2 as $row) {
        echo '<tr><td style="border: black solid 1px;">'.$row['name'].'</td></tr>';
    }
    echo '</table>';


    // 1.6 Alter the user HTML table to also display the client name they belong to

    echo '<h2> 1.6 Alter the user HTML table to also display the client name they belong to </h2>';

    $sql3 = "SELECT u.id, u.name, u.username, c.name as client FROM users u JOIN clients c ON u.client_id = c.id ORDER BY u.id";
    $result3 = $connection->query($sql3);

    ?>
    <!-- table>thead>tr>th{ID}+th{Name}+th{Username}+th{Client}^tbody>  -->
    
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Username</th>
                <th>Client</th>
            </tr>
            <tbody>
                <?php foreach ($result3 as $row) { ?>
                    <tr>
                        <td><?= $row['id']?></td>
                        <td><?= $row['name']?></td>
                        <td><?= $row['username']?></td>
                        <td><?= $row['client']?></td>
                        <td><input type="button" value="delete" onClick="<?php deleteFromTable() ?>"></td>
                    </tr>
                <?php }?>
            </tbody>
        </thead>
    </table>

    table>thead>tr>th{ID}+th{Name}+th{Username}+th{Client}^tbody

    <?php 


    // $connection->die();    
    
    // Bonus: Add a delete button to each user in the table which allows us to delete that user
    function deleteFromTable() {
        echo 'Trying to delete';
    }