<?php

include '../helpers/prettydump.php';

/*
*   1.0 Lav en funktion der kan tælle antallet af vokaler i en string (a, e, i, o, u, y, æ, ø, å)
*   F.eks: count_vowels('Hello World') == 3
*   F.eks: count_vowels('wtf?') == 0
*/
function count_vowels($string) {

    $vowels_array = array('a','e','i','o','u','y','æ','ø','å');
    pretty_dump($string);

    //TODO: foreach char in $string, see if it is in $vowels_array
    
    //// $string_array = explode(' ', $string);
    $string_array = str_split($string);
    $vowels_count = 0;

    foreach ($string_array as $key => $value) {
        
        //Hvad er nu key og value?
        // echo 'KEY:';
        // pretty_dump($key);
        // echo 'VALUE:';
        // pretty_dump($value);

        if(in_array($value, $vowels_array)){
            $vowels_count++;
        }
    }
    // pretty_dump($string_array);

    //PRINT RESULT
    echo '<h2>Der er <span style="color:green;">'. $vowels_count . '</span> vokaler i ' . $string . '</h2>';
}

//PRINT THE RESULTS
count_vowels('Hello World');
count_vowels('wtf');

//! Virker kun på engelsk
count_vowels('rød grød med fløde');




/* 
*
*   1.1 Modificer nu din funktion med et ekstra parameter, som hvis er true, returnerer den vokal der optræder flest gange. 
*       F.eks. count_vowels('Hello World', true) == 'o'
*       F.eks: count_vowels('Hello World', false) == 3
*
*/

function count_vowels_greatest($string, $return) {}