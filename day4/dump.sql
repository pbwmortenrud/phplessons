#
# TABLE STRUCTURE FOR: clients
#

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

INSERT INTO `clients` (`id`, `name`) VALUES (1, 'Douglas-Nader');
INSERT INTO `clients` (`id`, `name`) VALUES (2, 'Okuneva-Mertz');
INSERT INTO `clients` (`id`, `name`) VALUES (3, 'Dooley-Mante');
INSERT INTO `clients` (`id`, `name`) VALUES (4, 'Gleason Inc');
INSERT INTO `clients` (`id`, `name`) VALUES (5, 'Gutkowski Group');
INSERT INTO `clients` (`id`, `name`) VALUES (6, 'Lindgren, Cassin and Cremin');
INSERT INTO `clients` (`id`, `name`) VALUES (7, 'Deckow-Rempel');
INSERT INTO `clients` (`id`, `name`) VALUES (8, 'Bechtelar Group');
INSERT INTO `clients` (`id`, `name`) VALUES (9, 'Kuhn, Parker and Hermann');
INSERT INTO `clients` (`id`, `name`) VALUES (10, 'Leuschke, Herman and Swift');
INSERT INTO `clients` (`id`, `name`) VALUES (11, 'Reynolds-Legros');
INSERT INTO `clients` (`id`, `name`) VALUES (12, 'Satterfield-Bode');
INSERT INTO `clients` (`id`, `name`) VALUES (13, 'Kautzer, Zemlak and Kohler');
INSERT INTO `clients` (`id`, `name`) VALUES (14, 'Boehm LLC');
INSERT INTO `clients` (`id`, `name`) VALUES (15, 'Lemke-Hane');
INSERT INTO `clients` (`id`, `name`) VALUES (16, 'Mitchell-White');
INSERT INTO `clients` (`id`, `name`) VALUES (17, 'Hoeger Inc');
INSERT INTO `clients` (`id`, `name`) VALUES (18, 'Romaguera-Bode');
INSERT INTO `clients` (`id`, `name`) VALUES (19, 'Daugherty, Skiles and Schoen');
INSERT INTO `clients` (`id`, `name`) VALUES (20, 'Doyle-Schneider');


#
# TABLE STRUCTURE FOR: users
#

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `client_id` int(9) unsigned NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

ALTER TABLE `users` ADD FOREIGN KEY (`client_id`) REFERENCES `clients`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (1, 'goyette.rosemary', 'Jamir Langworth', 1, 'cb47a44a097b74a9b48ba48958380984eb97b90b');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (2, 'shackett', 'Paolo Feest', 2, '0cb4fb9a4a94c34f44b55a2df9c63285e66d26f9');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (3, 'mertie.friesen', 'Felix Halvorson', 3, '21d92cb4a168eff420964f5822b9b39ae1e947c6');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (4, 'rlindgren', 'Dr. Gillian Quigley', 4, 'b4ee7324ab2568435bb0c9b0176821b17ed4dcc6');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (5, 'lakin.gus', 'Prof. Arnaldo Pfannerstill DVM', 5, '5e5ea373ef4f04fd9150066b0bd17face4d41bec');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (6, 'hank51', 'Mr. Jameson McDermott DDS', 6, 'ca89a718f0a4e04a72c1d6caf1342a281067f2bf');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (7, 'augusta.herzog', 'Billy Larson', 7, '15cb28b0f0cb14d5c8c15269932e3a8a541ea4c6');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (8, 'wilmer79', 'Dr. Elvie Kohler V', 8, '45ada3037a795e38e4b5b0b0b02aa72fd713870d');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (9, 'wayne52', 'Dr. Ewell Hammes', 9, 'c79dc2f86c5fc54695118748b2f73634df55fca4');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (10, 'gmohr', 'Mrs. Janis Kuphal', 10, '425079f49eecfdbf045bd5c0f3060366b0d51944');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (11, 'qmarquardt', 'Ms. Meta Lehner DDS', 11, '918aad2b5dff40eda6b8a435c01fd7c367d0dc32');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (12, 'dovie04', 'Fabian Rowe', 12, 'b96a9e2662bb50f0d25aa5de041a901ceed0c923');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (13, 'kassandra41', 'Gideon Langosh', 13, 'a20e680b217b77dcd46b9177479a90a2626ea6c3');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (14, 'feest.sanford', 'Aleen Prohaska', 14, '22f9d6540d7c4bc9ca5368090fcac756b70e4f8f');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (15, 'lyric.schuppe', 'Adolph Deckow IV', 15, '7b12d139529b528d02b8c6281e8f5a4abdcb8a03');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (16, 'damore.keeley', 'Pearlie Collins', 16, 'a61107f9ec22870a46d0c88eea6727d550ecdd3a');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (17, 'matteo.johnston', 'Prof. Buck Lemke', 17, '54a375d22d126fe671c1e36df3c9e742897e598f');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (18, 'jettie28', 'Rudolph Kuphal', 18, '7f04727e1db35db4fc43ce8475548704f8685f64');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (19, 'terence.swift', 'Nikita Rohan Sr.', 19, 'c8116bbde693e0f84d4ae253f0e680ed1d116974');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (20, 'pcassin', 'Prof. Cory Powlowski', 20, 'fc266c0cb872e61ee16eeab414e6d4135bf26cb0');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (21, 'danika.gutmann', 'Kaci Jacobi', 1, '2d34803a08582f22b87e72d554a513c6e4acb338');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (22, 'sortiz', 'Corene Mayer', 2, 'ac252fb7135d0fdda3a77b088f4e5dd95328c842');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (23, 'purdy.rudolph', 'Gordon Jaskolski', 3, '74db503c880c3fd3bbf24078df091901ac74909d');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (24, 'wiegand.mia', 'Prof. Juliet Lang I', 4, '2e25711a659d5f61cf7fc72f69bed61c08457dd1');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (25, 'horace79', 'Ms. Abbey Haag', 5, 'a8858212e5246f758a9b07504ff9ed0353a6b632');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (26, 'hettinger.wyman', 'Maudie Mayer IV', 6, 'a5de48c182f2f33476862c7b022b2550f433ba7e');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (27, 'iwaelchi', 'Dock Braun', 7, '7696a90606f4bc6021d0986a4df13359f8c9a67a');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (28, 'dereck42', 'Dr. Jaden Emmerich DDS', 8, '501e99c75ed214d78bda38112811492f90832982');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (29, 'jayme.eichmann', 'Lane Stoltenberg PhD', 9, '0d46638bb93652e4a3c651918a9fa9f738101f4a');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (30, 'rita79', 'Mr. Kirk Zboncak DDS', 10, 'a9fb0f7bf937710e7eaeeab20ed9b3f3620750ad');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (31, 'walton29', 'Cesar Reichert V', 11, '7ed1a982764656524ea3bd3eb932884c53a6bd24');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (32, 'wiza.dahlia', 'Joanny Stiedemann', 12, 'd69e40e6a23d4b1d5db1292cc347dc392e1b6f50');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (33, 'ernest81', 'Miss Rachelle Feest', 13, '7ef912fa1cc00f42d70982f4abdd929b2fb1c039');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (34, 'sauer.peter', 'Mr. Mortimer Moen', 14, '0602edbc9573b193964530b7c1e17d0a0898f216');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (35, 'marvin.mason', 'Owen Morissette', 15, 'f91a0f1d2b7af4b5561a78d537ba0061b9fd0091');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (36, 'utreutel', 'Alfonso Pfannerstill', 16, 'b1385fbf9fb5cca137c989233054412a80e7c66f');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (37, 'cronin.milan', 'Skyla Ebert', 17, '2eac54e7399b8f8ebeba7f48713e9c9e1498728c');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (38, 'josinski', 'Prof. Eileen Yundt', 18, 'dcb37df20c2565ace6960909830cb43f6f7c8f0e');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (39, 'brakus.nicole', 'Justen Carter', 19, '79ad8c84b2185158aa727bfc5b060e841ecdb59a');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (40, 'jcummerata', 'Vivian Kozey', 20, '379b27fa64c3b79186714160439e0fd6a8d10aef');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (41, 'fahey.owen', 'Amber Rogahn', 1, '0690a62ce2c52c36702f9d368d32ef6eee813b30');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (42, 'mireya59', 'Prof. Einar Mitchell DVM', 2, '88ece82e76d6876b90c1fbb915efea546af586e3');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (43, 'elda.christiansen', 'Kennedy Corkery', 3, '8e80bc00551509e9e12c878401519e5f63312cee');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (44, 'frolfson', 'Eulah Trantow II', 4, 'b9fe7c1f0282b8c76a2b29e619bbc65108b68ca7');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (45, 'dlittle', 'Mr. Pietro Veum', 5, '6e5f9e26af3ce695bf6eff51faf5a79b45d04ae0');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (46, 'jakayla36', 'Ms. Sophia Heidenreich', 6, '02c06ce639b88c72c04ea15b1bf8936aa80db166');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (47, 'streich.llewellyn', 'Prof. Cordelia Miller II', 7, '3d35cfc44fe952f0fa81df48714c7f77f70977d1');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (48, 'tveum', 'Mrs. Lauriane Labadie', 8, '65fb30a3b59db4411b43b457a40d9de9f56cb6ef');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (49, 'aufderhar.irma', 'Ruthie Ondricka', 9, '2f67570335704099d506a69df7957ff206655047');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (50, 'noe07', 'Aurelie Treutel', 10, '947938100590d16306c2604861bfc5be8a9411f0');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (51, 'ckshlerin', 'Ursula Fahey', 11, '2b406ccafd278e3f339fc6a09369eabf86645ae2');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (52, 'rippin.cristobal', 'Adrianna Casper DVM', 12, 'b58aa967a1c7114f74b1819f9590256ca9359252');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (53, 'uhirthe', 'Darian Carroll', 13, 'f9fca0283ece7182a6a734c79c037347a25a3fb8');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (54, 'rahsaan.blick', 'Dr. Aaliyah Schumm', 14, '8d35761fed09e7067c1db8842fa09104bd04d810');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (55, 'susana.thompson', 'Miss Molly Huels', 15, '94b69d7dc8c1d4dea8408c3ce6cbad187418e1cb');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (56, 'ulices.leuschke', 'Mr. Guiseppe Schroeder DVM', 16, '570823ac10b64f66db8c7b5fb450efc256b5361e');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (57, 'lonnie87', 'Carleton Oberbrunner', 17, '0b00c03c89a96c7164cd9d81d6202a5759cd1916');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (58, 'rubye57', 'Johanna Franecki', 18, '7b8f981dae2756507f54d1d0353864687f9dfeeb');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (59, 'umclaughlin', 'Rosendo Konopelski Jr.', 19, '4062a7c04c115cf438afffca411bfa666cac87ba');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (60, 'sally.adams', 'Katherine Gutmann', 20, 'dce9334dd81557a2cd7b816d0c1d16a624329e1b');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (61, 'oupton', 'Van Hilll', 1, '0e12b00e58731bd673f271e3f412bbd9c85ea3ee');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (62, 'mclaughlin.manley', 'Destiney Keeling', 2, 'de7795f5b8225005c0ac34edc6d6f2015a2bb5ef');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (63, 'taryn.shields', 'Glenda Huel I', 3, '5c57947045e4827b1e00eaefe3fe4d33bbc6a567');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (64, 'cleveland.emmerich', 'Elton Romaguera', 4, 'dc429126f2075c73671f6d3be0240a473f5d2027');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (65, 'frank82', 'Miss Halie Littel', 5, '0bd748fb468244cb5ddc0f40a8f388c2b1ffc376');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (66, 'samara06', 'Rolando Walsh Jr.', 6, 'eaf3fc4a8b1183306ac73d3eeec14dfe8904b83a');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (67, 'vwaelchi', 'Dayton Glover MD', 7, '5dd5a61b47f95e60d02cc830dd000bcbc626459d');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (68, 'wyman.albert', 'Daren Smitham', 8, '66a95847a5f5485fba302f31b4c6e62b9cc36a9d');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (69, 'cristina.baumbach', 'Nelda Kirlin Sr.', 9, '07e2c01f59573352f518466a72cb2a7e3df58f6f');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (70, 'pietro.heller', 'Susanna Schamberger V', 10, '1e125486a29449575dd4d2f6b90d54b90368dad9');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (71, 'dwhite', 'Dr. Arnulfo McGlynn DDS', 11, 'b061dfdbfa44abc5ad9d514ba9a25e12726dd198');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (72, 'vreynolds', 'Furman Hoeger', 12, '2dcf445e485f94f936b57986fd779e95dbea4d92');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (73, 'alberta61', 'Dr. Salvador Botsford', 13, 'b58312dcf4a447fafd044c00b781c6c2f5cffedc');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (74, 'gislason.kailey', 'Serena Konopelski', 14, '7d2d0a5b9879b6bdadff0d4e739652b3b7c885db');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (75, 'casper.friesen', 'Mrs. Cleora Anderson I', 15, 'aa66f702fcb801bd5d707025dbccfe992d809157');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (76, 'obradtke', 'Friedrich OReilly', 16, 'ad917d8f8ad89ef5a84240106d328222cbd69dbe');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (77, 'orosenbaum', 'Miss Antonetta Dooley Sr.', 17, 'ed3e10a81d9a4c96667bf50df6f0b040818b7880');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (78, 'bkeeling', 'Ms. Meagan Konopelski', 18, '9ef7ce1d1ff477645b7b29076c144889c2f2fcdc');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (79, 'gtorphy', 'Kristy Strosin Sr.', 19, '40202331c7f51567ebd543ef10b160615372d0a7');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (80, 'marvin.carli', 'Prof. Kamille Ullrich Jr.', 20, '9c95afa8266567822b19a71f758a4a3613d1ac09');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (81, 'witting.braden', 'Jaquan Pollich', 1, '5302806a6d3c28c4aac85d4d131574ea14b666a9');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (82, 'hellen.hermann', 'Prof. Zola Weissnat', 2, '9a4f893ce359b8bd4c753eec2004973131db8ef7');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (83, 'jblock', 'Prof. Devyn Bashirian DVM', 3, '3576c56c503d5c51ba8d586697325acae975fcde');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (84, 'virgie97', 'Stefanie Torp', 4, '0731037666620174a8e77b4456e78be07cac200e');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (85, 'shannon.purdy', 'Miss Elissa Runolfsdottir', 5, '7be0b47776646c362319caae94b26544144a8688');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (86, 'kattie77', 'Gladyce Stoltenberg', 6, '8c1c4d36bd2a5eaa4e34e8f9b94c2b9183d34e96');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (87, 'kaylee.cronin', 'Jo Fahey', 7, '1353637fa45d47d091c3f33a024c1c75611f132c');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (88, 'jaskolski.celia', 'Elody Corwin', 8, 'e5067f388a6e26b2ffb58a691b746d700301f0fd');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (89, 'ythiel', 'Clifford Langworth', 9, 'fad9a7321597b5763357b6cc79dccea451343ad2');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (90, 'hope.bahringer', 'Peter Wolff', 10, '8ac5f674610d025b86672a774dba046f7c561933');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (91, 'paucek.mauricio', 'Gordon Medhurst', 11, 'e3a772883dfb833146f86ca92647676c7bdc70c8');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (92, 'adams.vito', 'Shaylee Corkery', 12, 'b042956203387273f2dbd30c1dd21d2ae81b0c09');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (93, 'hwisoky', 'Ms. Vallie VonRueden', 13, '1488ef6a7f78e80da40fced07f99b540627fa50c');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (94, 'laurel19', 'Ned Prohaska', 14, 'fd408d95544e8542b7aff046cc34092eec96d53f');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (95, 'oweimann', 'Loy Pfeffer', 15, 'a21ee2d38b2c198492ed40959433dc2fa5602b7c');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (96, 'dhilll', 'Dr. Uriel Reynolds', 16, '11c7542fdf5b3b699036d0a08eebd0cee9c05ef0');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (97, 'frederick02', 'Ashley Johnston', 17, 'a72cdeee6f9cf95ffd975d679ab7d7b800b105ea');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (98, 'ufriesen', 'Stephen Gorczany III', 18, 'adc5d6f38de1af9df670f26b679b339e2ba1e9cc');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (99, 'amparo52', 'Tobin Schumm', 19, '2db1b0a4817f3daa0657bee7bf4691bc614b7d61');
INSERT INTO `users` (`id`, `username`, `name`, `client_id`, `password`) VALUES (100, 'stephania.cassin', 'Dr. Verla Johns MD', 20, '0f5bf5d044291b459e8eedd9f37dc8e22701eecb');


