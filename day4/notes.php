<?php

/* MIKKELS LØSNING */

function count_vowels_mikkel($string) {

    $count = 0;
    
    $string = strtolower($string);

    $string_array = str_split($string);

    foreach($string_array as $letter) {
        $count += (strpos('aeiouyæøå', $letter) !== false) ? 1 : 0;
    }

    return $count;
}
 /*SLUT PÅ MIKKELS LØSNING*/


 //$result_array = $result->fetch_all(MYSQLI_ASSOC);

 ?>